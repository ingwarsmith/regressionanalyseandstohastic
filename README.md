# RegressionAnalyseAndStohastic

***

# Статистический анализ

## Regression Analyse And Stohastic (rans)
Первый вариант реализации генератора псевдослучайных чисел, распределённых по треугольному и нормальному распределениям

## Build
Запустите all-build.sh

## Usage
В папке bin после успешной сборки будет находиться исполняемый файл rans_base

## Examples
Обработанный результат (гистограммы распределений) в изображениях PNG в каталоге result_gistograms

## Author
E-mail: ingwarsmith@yandex.ru

## License
Freeware / open-source

## Project status
Версия 0.1: нормальное распределение смоделировано достаточно грубо.

