#!/bin/sh
cmake -S . -B build
if [ $? -eq 0 ]
then
    cd build
    make
else
    echo "Cmake work with error(s)" >&2
    exit 2
fi
cp rans_base ../bin/


