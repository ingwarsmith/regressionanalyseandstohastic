#include <iostream>

#include "triangulardistribution.h"
#include "normaldistribution.h"

int main()
{
    TriangularDistribution dst_t(23, 2370, 2150);
    NormalDistribution dst_n(23, 2370);
    auto res_t = dst_t.generate(24000);
    auto res_n = dst_n.generate(24000);
    std::size_t number = 1;
    std::size_t sz = res_t.size();
    std::cout << "row;triangular_dist;normal_dist" << std::endl;
    for (std::size_t i = 0; i < sz; ++i, ++number)
    {
        std::cout << number << ';' << res_t[i] << ';' << res_n[i] << std::endl;
    }
    return 0;
}
