#include "normaldistribution.h"

#include <algorithm>
#include <random>

///

std::vector<specreal> NormalDistribution::generate(std::size_t count_values)
{
    std::vector<specreal> result;

    if (count_values < 500)
    {
        m_invalid = true;
        m_error_text = "insufficient values count: must be more than 500";
        return result;
    }

    std::random_device rd;
    std::mt19937 gen(rd());

    /// p(x) - 8 intervals:
    /// 0.15%
    /// 2.35%
    /// 13.5%
    /// 34%
    /// 34%
    /// 13.5%
    /// 2.35%
    /// 0.15%

    std::vector<std::pair<specreal, specreal>> intervals(8); /// interval: [min, max)
    std::vector<std::size_t> count_values_by_intervals(8);
    std::vector<specreal> parts { 0.15, 2.35, 13.5, 34., 34., 13.5, 2.35, 0.15 };
    auto range = m_max - m_min;
    auto one_interval = range / 8.;
    auto next_interval_begin = m_min;
    auto c_v = static_cast<specreal>(count_values);
    std::size_t idx = 0;
    std::size_t count_all_values_generated = 0;
    for (auto & interval : intervals)
    {
        interval = std::make_pair(next_interval_begin, next_interval_begin + one_interval);
        next_interval_begin += one_interval;
        count_values_by_intervals[idx] = static_cast<size_t>(std::round(c_v * parts[idx] / 100.));
        count_all_values_generated += count_values_by_intervals[idx];
        idx++;
    }
    int64_t diff_count_values = static_cast<int64_t>(count_values) - static_cast<int64_t>(count_all_values_generated);
    if (diff_count_values > 0)
    {
        idx = 0;
        while (diff_count_values)
        {
            count_values_by_intervals[idx]++;
            diff_count_values--;
            idx++;
            if (idx == 8) idx = 0;
        }
    }
    else if (diff_count_values < 0)
    {
        idx = 0;
        while (diff_count_values < 0)
        {
            count_values_by_intervals[idx]--;
            diff_count_values++;
            idx++;
            if (idx == 8) idx = 0;
        }
    }
    idx = 0;
    for (auto & interval : intervals)
    {
        std::uniform_real_distribution<> dis(interval.first, interval.second);
        for (std::size_t n = 0; n < count_values_by_intervals[idx]; ++n)
        {
            result.push_back(dis(gen));
        }
        idx++;
    }

    return result;
}
