#include "triangulardistribution.h"

#include <algorithm>
#include <random>

///

std::vector<specreal> TriangularDistribution::generate(std::size_t count_values)
{
    std::vector<specreal> result;

    if (count_values < 500)
    {
        m_invalid = true;
        m_error_text = "insufficient values count: must be more than 500";
        return result;
    }

    std::random_device rd;
    std::mt19937 gen(rd());

    /// p(x) =
    /// 0, x < min
    /// 2*(x - most_prob) / ((max - min)*(most_prob - min)), x ~ [min, most_prob]
    /// 2*(max - x) / ((max - min)*(max - most_prob)), x ~ (most_prob, max]
    /// 0, x > max

    std::vector<std::pair<specreal, specreal>> intervals; /// interval: [min, max)
    std::vector<std::size_t> count_values_by_intervals(100);
    auto range = m_max - m_min;
    auto one_interval = range / 100.;
    auto next_interval_begin = m_min;
    std::size_t idx_interval_with_most_prob = 0;
    while (intervals.size() < 100)
    {
        intervals.push_back(std::make_pair(next_interval_begin, next_interval_begin + one_interval));
        next_interval_begin += one_interval;
        if (idx_interval_with_most_prob == 0 && m_most_prob <= intervals.back().first)
        {
            idx_interval_with_most_prob = intervals.size() - 1;
        }
    }
    std::size_t count_values_interval_average = count_values / intervals.size();
    count_values_by_intervals[idx_interval_with_most_prob] = count_values_interval_average * 2;
    auto most_prob_count_dbl = static_cast<specreal>(count_values_by_intervals[idx_interval_with_most_prob]);
    std::size_t idx = idx_interval_with_most_prob - 1;
    auto delimiter = (m_max - m_min) * (m_most_prob - m_min);
    std::size_t count_all_values_generated = count_values_by_intervals[idx_interval_with_most_prob];
    while (idx > 0)
    {
        auto v = most_prob_count_dbl *
                ((intervals[idx].second + intervals[idx].first)/2. - m_min) / delimiter * 100.
                * ((intervals[idx].second - intervals[idx].first));
        count_values_by_intervals[idx] = static_cast<size_t>(std::round(v));
        count_all_values_generated += count_values_by_intervals[idx];
        idx--;
    }
    auto v0 = most_prob_count_dbl *
            ((intervals[idx].second + intervals[idx].first)/2. - m_min) / delimiter * 100.
            * ((intervals[idx].second - intervals[idx].first));
    count_values_by_intervals[idx] = static_cast<size_t>(std::round(v0));
    count_all_values_generated += count_values_by_intervals[idx];
    idx = idx_interval_with_most_prob + 1;
    delimiter = (m_max - m_min) * (m_max - m_most_prob);
    while (idx < intervals.size())
    {
        auto v = most_prob_count_dbl *
                (m_max - (intervals[idx].second + intervals[idx].first)/2.) / delimiter * 100.
                * ((intervals[idx].second - intervals[idx].first));
        count_values_by_intervals[idx] = static_cast<size_t>(std::round(v));
        count_all_values_generated += count_values_by_intervals[idx];
        idx++;
    }
    int64_t diff_count_values = static_cast<int64_t>(count_values) - static_cast<int64_t>(count_all_values_generated);
    if (diff_count_values > 0)
    {
        idx = 0;
        while (diff_count_values)
        {
            count_values_by_intervals[idx]++;
            diff_count_values--;
            idx++;
            if (idx == 100) idx = 0;
        }
    }
    else if (diff_count_values < 0)
    {
        idx = 0;
        while (diff_count_values < 0)
        {
            count_values_by_intervals[idx]--;
            diff_count_values++;
            idx++;
            if (idx == 100) idx = 0;
        }
    }
    idx = 0;
    for (auto & interval : intervals)
    {
        std::uniform_real_distribution<> dis(interval.first, interval.second);
        for (std::size_t n = 0; n < count_values_by_intervals[idx]; ++n)
        {
            result.push_back(dis(gen));
        }
        idx++;
    }

    return result;
}

bool TriangularDistribution::invalid()
{
    return m_invalid;
}

const std::string &TriangularDistribution::last_error()
{
    return m_error_text;
}
