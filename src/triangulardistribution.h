#ifndef TRIANGULARDISTRIBUTION_H
#define TRIANGULARDISTRIBUTION_H

#include <vector>
#include <string>

typedef double specreal;

struct TriangularDistribution
{
    TriangularDistribution(specreal min_value, specreal max_value, specreal most_probable_value)
        : m_min(min_value), m_max(max_value), m_most_prob(most_probable_value), m_invalid(false) {}

    std::vector<specreal> generate(std::size_t count_values);
    bool invalid();
    const std::string & last_error();

private:
    specreal m_min;
    specreal m_max;
    specreal m_most_prob;
    bool     m_invalid;
    std::string m_error_text;
};

#endif // TRIANGULARDISTRIBUTION_H
